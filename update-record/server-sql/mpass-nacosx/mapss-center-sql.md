```sql
CREATE TABLE `mpass`.`application_config` (
`fd_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`fd_content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
`fd_tenant_id` int(11) NULL DEFAULT NULL,
PRIMARY KEY (`fd_id`) 
);


CREATE TABLE `mpass`.`design_element` (
`fd_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`fd_app_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
`fd_label` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_md5` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_message_key` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_module` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
PRIMARY KEY (`fd_id`) 
);


CREATE TABLE `mpass`.`sys_empty` (
`fd_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`fd_tenant_id` int(11) NULL DEFAULT NULL,
PRIMARY KEY (`fd_id`) 
);


CREATE TABLE `mpass`.`system_config` (
`fd_id` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`fd_application` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_label` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_profile` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fd_value` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
PRIMARY KEY (`fd_id`) 
);



```